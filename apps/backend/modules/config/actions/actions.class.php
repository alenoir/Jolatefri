<?php

require_once dirname(__FILE__).'/../lib/configGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/configGeneratorHelper.class.php';

/**
 * config actions.
 *
 * @package    jolatefri
 * @subpackage config
 * @author     Antoine Lenoir
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class configActions extends autoConfigActions
{
}

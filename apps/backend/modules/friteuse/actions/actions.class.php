<?php

require_once dirname(__FILE__).'/../lib/friteuseGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/friteuseGeneratorHelper.class.php';

/**
 * friteuse actions.
 *
 * @package    jolatefri
 * @subpackage friteuse
 * @author     Antoine Lenoir
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class friteuseActions extends autoFriteuseActions
{
}

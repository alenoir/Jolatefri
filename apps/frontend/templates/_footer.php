<footer id="footer">
	<div id="footer-top"></div>
	<div id="footer-content">
		<ul>
			<li class="facebook col">
				<h4>
					Rejoignez les fans de Jolatefri :
				</h4>
				<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like-box" data-href="http://www.facebook.com/Jolatefri.video" data-width="292" data-show-faces="true" data-stream="false" data-header="true"></div>
			</li>
			<li class="friends col">
				<h4>
					Les amis de Jolatefri :
				</h4>
					
				<ul class="list-footer">
					<li>
						<a target="_blank" href="http://www.mickeymouse.fr">Disneyland Paris</a>
					</li>
					<li>
						<a href="http://www.pluggd.fr" target="_blank">Pluggd - Tendances Digitales</a>
					</li>
				</ul>
			</li>
			<li class="col">
				<h4>
					Vidéos :
				</h4>
				<?php include_component('video', 'listFooter');?>
			</li>
		</ul>
		<div style="clear:both;"></div>
		<p id="copiright">Copyright © 2010 - Jolatefri | Si vous êtes l'auteur d'un élément de ce site, vous pouvez si vous le souhaitez, le modifier ou le supprimer contactez nous par <a href="mailto:admin@jolatefri.com">mail</a> </p>

	</div>
	
	
</footer>
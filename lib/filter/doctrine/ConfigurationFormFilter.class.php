<?php

/**
 * Configuration filter form.
 *
 * @package    jolatefri
 * @subpackage filter
 * @author     Antoine Lenoir
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ConfigurationFormFilter extends BaseConfigurationFormFilter
{
  public function configure()
  {
  }
}

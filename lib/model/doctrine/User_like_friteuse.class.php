<?php

/**
 * User_like_friteuse
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    jolatefri
 * @subpackage model
 * @author     Antoine Lenoir
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class User_like_friteuse extends BaseUser_like_friteuse
{
	public function getModelName()
  	{
    	return 'User_like_friteuse';
  	}
}
